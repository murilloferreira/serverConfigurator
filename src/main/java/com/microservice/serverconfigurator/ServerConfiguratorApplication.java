package com.microservice.serverconfigurator;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.config.server.EnableConfigServer;

@SpringBootApplication
@EnableConfigServer
public class ServerConfiguratorApplication {

	public static void main(String[] args) {
		SpringApplication.run(ServerConfiguratorApplication.class, args);
	}
}
